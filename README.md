# OpenFOAM SIG: Research Software Engineering

This is the documentation repository of the OpenFOAM Special Interests Group: Research Software Engineering.

You can find more details regarding the group in the [OpenFOAM Wiki page of the group](https://wiki.openfoam.com/Research_Software_Engineering_Special_Interest_Group).

The purpose of this repository is to document knowledge obtained via the activities of the group.
