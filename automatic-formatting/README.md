# An attempt to automatic formatting of OpenFOAM code

This repository serves as a minimal working example to experiment with various options for automatic code formatting / indentation in OpenFOAM. Tweak the settings or add configuration files for your favorite tool via a pull request. The smaller the diff of the example file, the better.

Many people have already tried to find a solution. See the [central issue in the OpenFOAM GitLab](https://develop.openfoam.com/Development/openfoam/-/issues/1634).

Ideally, the provided solution should follow the [OpenFOAM Style Guide](https://develop.openfoam.com/Development/openfoam/-/wikis/coding/style/style) as much as possible.

The example code file (`dynamicIndexedOcttree.C`) is [part of OpenFOAM](https://develop.openfoam.com/Development/openfoam/-/blob/a9b451b3e4f98933b8b3f7df13066f5537de5086/src/OpenFOAM/algorithms/dynamicIndexedOctree/dynamicIndexedOctree.C), licensed under GPLv3.

## Contribute

Open an [issue](https://gitlab.com/openfoam/community/sig-research-software/-/issues) describing what you would like to see changed, including diffs/images of examples.

If you want to change some option, submit a [merge request](https://gitlab.com/openfoam/community/sig-research-software/-/merge_requests). You can fork the repository, or you can ask to be added directly here, if you want to work more actively. In the merge request, change the `.clang-format` file and modify the `*.formatted` files, so that we can keep track of what each option changes.

## FAQ

### Why is automatic formatting important?

Multiple reasons:

- Reduce merge conflicts
- Save developers' time, energy, and focus (OpenFOAM or derived projects)
- Make contributing much easier
- Stop worrying and love the ~~bomb~~ formatter

This is a very important step towards encouraging and being able to merge community contributions.

### What kind of automation are we talking about?

Examples:

- You push to GitLab and the CI tells you immediately if the code is correctly formatted or not.
- Before you make a commit, a Git pre-commit hook tells you if you are trying to commit incorrectly formatted code, or even formats the code for you.
- Any editor/IDE automatically formats on save, based on the common config.

### Do we need to stick to the existing style guide?

Ideally, yes. But the main reason is familiarity.

Potentially in-house tools that assume a specific format could also break (hypothetical scenario - are there any?).

### Will there be merge conflicts?

When first adding a formatter, there will probably be a large diff. Even if we perfectly follow the style guide, the formatter may "fix" existing inconsistencies. But every following contribution should simply be formatted with the same script.

Existing merge requests could just apply the same format automatically before merging. Updating the format will render existing review comments on merge requests as outdated. For this reason, the ideal scenario is that any major pull requests are merged before the codebase is first reformatted.

## Option: Clang-format

You can get recent versions of clang-format from PIP: `pip3 install --user clang-format==17.0.6`.

Format using:

```shell
cd example_code
clang-format dynamicIndexedOctree.C > dynamicIndexedOctree.C.formatted
```

[Clang-Format Style Options](https://clang.llvm.org/docs/ClangFormatStyleOptions.html)

Related discussions:

- [Pull request in OpenFOAM-preCICE](https://github.com/precice/openfoam-adapter/pull/173), where the included `.clang-format` configuration file is extracted from (GPLv3).
- [Invitation to the LLVM community](https://discourse.llvm.org/t/showcasing-the-strengths-of-clang-format-to-the-openfoam-community/75762)

Some issues follow here (the code snippets are from an older state of the example file).

### Issue 1: Round braces in dedicated lines

Cannot tell clang-format that parentheses should open and close in dedicated lines.

```diff
- contents_.append
- (
-     autoPtr<DynamicList<label>>
-     (
-         new DynamicList<label>()
-     )
- );
---
+ contents_.append(
+     autoPtr<DynamicList<label>>(
+         new DynamicList<label>()));
```

**Upside:** Keeping the opening brace next to the name leads to IDEs understanding that e.g. `append` is a function and colorize it properly.

[Related discussion in the LLVM forum](https://discourse.llvm.org/t/clang-format-round-braces-in-dedicated-lines/77014)

### Issue 2: Spaces around arithmetic operators

Cannot tell clang-format that it should not add spaces around arithmetic operators. [Related StackOverflow](https://stackoverflow.com/questions/67245250/can-i-tell-clang-format-to-not-add-space-around-arithmetic-operators). Both spaces and no-spaces are actually allowed by the guidelines (and I prefer spaces).

```diff
- const point mid(0.5*(min+max));
---
+ const point mid(0.5 * (min + max));
```

**Upside:** Consistent formatting (single rule instead of either), more easily identifiable operands.

### Issue 3: Aligning operations

Blocks with multiple operations are still aligned, but not exactly in the same way:

```diff
-    if
-    (
-        bb.min()[0] >= bb.max()[0]
-     || bb.min()[1] >= bb.max()[1]
-     || bb.min()[2] >= bb.max()[2]
-    )
---
+    if (
+        bb.min()[0] >= bb.max()[0]
+        || bb.min()[1] >= bb.max()[1]
+        || bb.min()[2] >= bb.max()[2])
```

Maybe there is a way to fix the alignment. The opening/closing braces in the same lines is again issue 1.

### Issue 4: Space in streams

OpenFOAM always starts `<<` in the fourth column after the start of the indentation level, often leaving no space between the target stream and the operator.

```diff
- Pout<< "    iter:" << i
-     << " hit face:" << faceString(hitFaceID)
---
+ Pout << "    iter:" << i
+      << " hit face:" << faceString(hitFaceID)
```

**Upside:** The added space could make this more readable. It also looks less like fortran fixed format.

### Issue 5: Columns limit

I had to set `ColumnLimit: 0`. If I set it to `80`,  clang-format was formatting things that it shouldn't. I tried tuning the penalties, but I could not succeed. This leads to situations such as the following:

```diff
- nodes_[parentNodeIndex].subNodes_[octantToBeDivided]
-    = nodePlusOctant(sz, octantToBeDivided);
---
+ nodes_[parentNodeIndex].subNodes_[octantToBeDivided] = nodePlusOctant(sz, octantToBeDivided);
```

### Issue 6: Indentation in initializer list

Similarly to issue 1, no dedicated line for `:`:

```diff
- :
-    shapes_(shapes),
-    bb_(bb), 
---
+ : shapes_(shapes),
+   bb_(bb),
```

At least the `:` remains not indented.

## Option: c-mode

Mark Olesen maintains an OpenFOAM indentation rule in a [fork of the jed editor](https://github.com/olesenm/jed/blob/master/lib/cmode.sl#L1721).

To clarify:

- What are the limitations?
- What is the exact workflow?
- Can this be scripted?
