# Continuous Integration for OpenFOAM

This guide was started during the OpenFOAM RSE meeting of December 7, 2023.
However, it is not meant to be static: Contribute your knowledge via a merge request!

## GitLab CI

[GitLab CI documentation](https://docs.gitlab.com/ee/ci/)

Examples of project using GitLab CI:

- (todo)

## GitHub Actions

[GitHub Actions documentation](https://docs.github.com/en/actions)

Examples of project using GitHub Actions:

- [OpenFOAM-preCICE](https://github.com/precice/openfoam-adapter/tree/develop/.github/workflows)

## Running on HPC

To trigger HPC jobs from GitLab CI, FZ Jülich develops [Jacamar](https://apps.fz-juelich.de/jsc/hps/juwels/jacamar.html).

## See also

See also a related guide in [Reproducibility](https://gitlab.com/openfoam/community/sig-research-software/-/blob/main/guides/reproducibility.md) and [Contibuting workflows](https://gitlab.com/openfoam/community/sig-research-software/-/blob/main/guides/contributing-workflows.md).

The [Turbulence TC](https://gitlab.com/openfoam/community/tc-turbulence/turbulence-community) is working in integrating external projects via Git submodules.